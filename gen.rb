#!/usr/bin/env ruby
3.upto(50) do |tc|
  n =  1 + rand(1_000_000)
  input = []
  n.times { input << rand(1000000007) }
  infile = File.new(".in_#{tc}.txt", 'w')
  k = rand(n)
  infile.write("#{n} #{k + 1}\n#{input.join(" ")}\n")
  infile.close
  input.sort!
  outfile = File.new(".out_#{tc}.txt", 'w')
  outfile.write("#{input[k]}\n")
  outfile.close
  helpfile = File.new(".help_#{tc}.txt", 'w')
  helpfile.write("#{input.join("\n")}\n")
  helpfile.close
end
