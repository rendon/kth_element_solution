/* Copyright 2016 Rafael Rendón Pablo <rafaelrendonpablo@gmail.com> */
// region Template
#include <bits/stdc++.h>
using namespace std;
typedef long long           int64;
typedef unsigned long long  uint64;
const double kEps   = 10e-8;
const int kMax      = 1000;
const int kInf      = 1 << 30;
// endregion

int lowerCount(vector<int>& A, int v) {
    int count = 0;
    for (int a : A) {
        if (a < v) {
            count++;
        }
    }
    return count;
}

int upperCount(vector<int>& A, int v) {
    int count = 0;
    for (int a : A) {
        if (a <= v) {
            count++;
        }
    }
    return count;
}

int findKth(vector<int>& A, int k) {
    int low = *std::min_element(A.begin(), A.end());
    int high = *std::max_element(A.begin(), A.end()) + 1;
    int steps = 0;
    while (low < high) {
        steps++;
        int m = low + (high - low) / 2;
        int lc = lowerCount(A, m);
        int uc = upperCount(A, m);
        if (k < lc) {
            high = m;
        } else if (k >= uc) {
            low = m + 1;
        } else {
            return m;
        }
    }
    return -1;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    int n, k;
    cin >> n >> k;
    vector<int> A(n);
    for (int& a : A) {
        cin >> a;
    }
    // sort(A.begin(), A.end());
    // cout << A[k-1] << endl;
    cout << findKth(A, k - 1) << endl;
    return EXIT_SUCCESS;
}

